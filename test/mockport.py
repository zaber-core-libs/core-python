﻿import re
import pytest

import context
from zaber.serial import AsciiCommand, BinaryCommand, TimeoutError


class MockPort(object):
    def __init__(self):
        self._expectations = []
        self._writeBuffer = bytearray()
        self._readBuffer = bytearray()
        self._isOpen = False

    def expect(self, aCommand, aResponses=None):
        if (isinstance(aCommand, bytes)):
            regex = re.compile(aCommand)
        else:
            regex = re.compile(aCommand.encode())

        rawReplies = aResponses or []
        replies = []
        if (not isinstance(rawReplies, list)):
            rawReplies = [rawReplies]

        for raw in rawReplies:
            if (not isinstance(raw, bytes)):
                raw = raw.encode()

            replies.append(raw)

        self._expectations.append((regex, replies))

    def stuff_receive_buffer(self, aResponse):
        raw = aResponse
        if (not isinstance(raw, bytes)):
            raw = raw.encode()
        self._readBuffer.extend(raw)

    def _check_for_match(self):
        while ((len(self._expectations) > 0) and (len(self._writeBuffer) > 0)):
            match = self._expectations[0][0].match(self._writeBuffer)
            assert match is not None, "Expectation not matched: {} !~ {}" \
                .format(self._expectations[0][0].pattern, self._writeBuffer)

            self._writeBuffer = self._writeBuffer[match.end():]
            for reply in self._expectations[0][1]:
                self._readBuffer.extend(reply)

            self._expectations = self._expectations[1:]

    def validate(self):
        assert len(self._expectations) == 0, "%d expectations were not matched." % len(self._expectations)
        assert len(self._readBuffer) == 0, "Read buffer was not cleared."
        assert len(self._writeBuffer) == 0, "Extra data was written without an expectation"

    def write(self, aData):
        self._writeBuffer.extend(aData)
        self._check_for_match()
        return len(aData)

    def read(self, aCount):
        if (len(self._readBuffer) >= aCount):
            result = self._readBuffer[:aCount]
            self._readBuffer = self._readBuffer[aCount:]
            return bytes(result)

        raise TimeoutError("Not enough data in receive buffer when read() was called.")

    def readline(self, aCount=-1):
        line = bytearray()
        while ((aCount < 0) or (len(line) < aCount)):
            b = self.read(1)
            line.extend(b)
            if (b == b'\n'):
                break

        return line

    def inWaiting(self):
        return len(self._readBuffer)

    def flush(self):
        self._writeBuffer = bytearray()
        self._readBuffer = bytearray()

    def open(self):
        if (self._isOpen):
            raise AssertionError("Port opened twice.")

        self._isOpen = True

    def close(self):
        if (not self._isOpen):
            raise AssertionError("Port closed twice.")

        self._isOpen = False
