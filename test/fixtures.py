# A special file for pytest fixtures.
# All fixtures defined here are accessible to
# all test_*.py files in this directory.

import os
import pytest

from asciiserialwrapper import AsciiSerialWrapper
from binaryserialwrapper import BinarySerialWrapper
from mockport import MockPort


@pytest.fixture(scope="function")
def fake():
    return MockPort()


@pytest.yield_fixture(scope="function")
def asciiserial(fake):
    with AsciiSerialWrapper(fake) as s:
        yield s


@pytest.yield_fixture(scope="function")
def binaryserial(fake):
    with BinarySerialWrapper(fake) as s:
        yield s
